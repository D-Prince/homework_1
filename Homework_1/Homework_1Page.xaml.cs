﻿using Xamarin.Forms;

namespace Homework_1
{
    public partial class Homework_1Page : ContentPage
    {
        public Homework_1Page()
        {
            InitializeComponent();


            Content = new Label
            {

                Text = "Hi! My name is Damon Prince, I'm a CS major, and I should be graduating this semester! ",
                BackgroundColor = Color.Red,
                TextColor = Color.White,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };

            BackgroundColor = Color.Black;
        }
    }
}
